1.
가장 먼저 CMakeLists.txt는 비단 root만을 위한 건 아니니까 루트 없이 c++ 파일을 빌드하는 걸 해보자.

2. 백문이 불여일견 아래를 해보도록 하자.

mkdir build
cd build
ccmake ..  -> c -> g
(or cmake ../)
make -j4
./Main

3.
CMakeLists.txt 는 어딨지?  지금 폴더에 그냥 있지. 이게 조금 지저분하니까 3번 예제에서 다른 곳에 놓도록 하고

CMakeLists.txt를 잘 보면
1)루트에 관한 건 전혀 없고
2)#을 이용해 주석을 달 수 있다는 걸 알 수 있지.

4.
소스를 잘 살펴보면 learnclass.cxx가 메인이고 setvalues.cxx는 learnclass.h 에서 정의된 클래스에 포함된 function을 가지고 있는
그냥 조그만한 파일이야 -> 나중에 라이브러리 폴더에 넣으면 되겠지? 이런 것도 다 3번에서 해보고, 지금은 그냥 2번으로 넘어가자.