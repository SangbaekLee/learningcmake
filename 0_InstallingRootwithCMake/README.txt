1. CERN에서 루트 소스를 받기
(e.g. https://root.cern.ch/get-root-sources) 

2. 
홈 폴더에 root 소스를 가지고 있다고 치자.
예를 들면 압축을 푼 위치가
~/root-6-08.06
라고 치자.

3.
그러면
mkdir ~/root6
를 해서 빌드 폴더를 만듦

4.
이제, CMake를 이용해서 루트를 인스톨해보자.
** CMake란? https://stackoverflow.com/questions/25789644/difference-between-using-makefile-and-cmake-to-compile-the-code

즉, Cross-platform make file - 쓰기 쉽고 여러 환경에서 잘 작동함!

Homebrew를 이용해서 cmake를 업데이트하자! (http://brewformulas.org/Cmake)

4-1.
CMake를 이용해서 루트를 인스톨하는 방법은 아래에 잘 나와있음.
https://root.cern.ch/building-root

그런데, "flag" 라고 불리는 옵션이 많아지면 cmake를 쓰기가 상당히 불편해.
옵션을 많이 사용하지 않으면 별로 상관이 없어

예를 들면

$cmake ../root-6-08.06

아니면 옵션을 켠다고 해도 하나뿐이라던가

$cmake ../root-6-08.06
$cmake -DCMAKE_INSTALL_PREFIX=/tmp/root -P cmake_install.cmake


4-2.
그런데 옵션이 여러 개 생기면 어떻게 될까?
이럴 때 사용하는 게 바로
ccmake

ccmake ../root-6-08.06

을 사용하면 앞서 말한 옵션들이 주르륵 뜨고 거기에서 엔터를 눌러서 변경 가능해
예를 들면 c++11이 default인데 c++11 을 off 하고 c++14를 켠다던가,

파이썬의 path가 맥북 built-in python path로 되어있는데
homebrew 파이썬 path로 변경한다던가

명령어를 몰라도 이런 것들이 손쉽게 가능하지

4-3.
이제 이 다음에는 make -j4 && make install 하면 그 다음부터는 너가 아는 부분일거야

5.
그럼 이제 친숙한 루트 인스톨을 cmake로 해봤으니 감이 오겠지만

1)CMakeLists.txt 를 잘 짜서 source에 배열한다.
2)build 폴더를 만든다
3)ccmake ../($source_folder)
4) make -j4
5) 선택 - (make install)
6) 실행

이 주가 되겠지.

이런 복잡한 것들은 아래 쓰레드들을 보면서 천천히 익히고,
하나하나 CMakeLists.txt 를 어떻게 이용하는지 나머지 폴더 안의 예제를 통해 살펴보자.

https://stackoverflow.com/questions/8304190/cmake-with-include-and-source-paths-basic-setup
https://stackoverflow.com/questions/6352123/multiple-directories-under-cmake
https://stackoverflow.com/questions/17511496/how-to-create-a-shared-library-with-cmake