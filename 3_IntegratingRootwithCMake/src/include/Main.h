#ifndef __MAIN__
#define __MAIN__ 
#include "AnalysisLib.h"
#include "Roofitted.h"
// double_t* framefitted(TH1D* histo,Int_t foilnum);
// RooPlot* framefitted(TH1D* histo,Int_t foilnum);
// void SaveRooFig1D(int argc, RooPlot* histo[],int setlog, TString name);
// void NameSetting();

  Int_t narray;
  TString nameXYZ[3], namexz[3], nameyz[3], namer[3], namez[3], namex[3], namey[3];
  TCanvas *canvasA, *canvasB, *canvasH;
  TString filename;
  TFile *f;
  double_t centerx[3]= {0., 0., 0.,};
  double_t centery[3];
  double_t centerz[3];
  double_t t, theta;
  double_t radi;
  TH1D* histoX;
  TH1D* histoY;
  TH1D* histoZ;
  TH1D* histoEdep;
  TH1D* histoEk;
  TH1I* histofoilnum;
  TH1I* histoeventID;
  TH1I* histotracksize;
  TH1I* histotrackID;
  TH1I* histopID;
  TH1I* histohitsize;
  TH1I* histohitID;
  TH1D* histothetareco;
  TH2D* histothetareco2;
  TH2D* histothetareco3;
  TH1D* histosanity;
//For drawing
  TH3D* histoXYZ[3];
  TH2D* histoxz[3];
  TH2D* histoyz[3];
  TH1D* histor[3];
  TH1D* histoz[3];
//For restoring
  TH1D* histox[3];
  TH1D* histoy[3];
  //drawing trick
  TH1D* cache1D[1];
  TH2D* cache2D[1];
  Int_t countevent, counthit;
  Int_t nhit;
  Int_t checkfoil[3];
  TVector3 foilhit[3];
  double_t AverageX[3];
  double_t AverageY[3];
  double_t AverageZ[3];
  double_t AverageR[3];
  double_t TotEdep[3];
  // double_t AverageEk;
  Int_t notgoodcases;
  double_t theta_reco;
  double_t phi_reco;
  double_t radtodeg = 180./M_PI;
  Int_t binnumber;
  TVector3 foil1hit, foil2hit, foil3hit;
  RooPlot* frame[3];
  ofstream outfile;
  ofstream outfilefit;
  stringstream osmomentum, osangle;
  TString Momentum, Angle;
#endif  