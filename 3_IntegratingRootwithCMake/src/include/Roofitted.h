#ifndef ROOFITTED_H
#define ROOFITTED_H

#include "plugin.h"

class Roofitted {
  private:  
    TH1D* h[3];
  public:
    Roofitted(); // default constructor
    Roofitted(TH1D* histo[3], TString name, int xory, int draw); // constructor with argumnents.
    ~Roofitted();
    double_t value[3];
    double_t error[3];
    RooPlot* frameout[3];
    void Sethistofoilnum(TH1D* histo[3]);
    void framefitted(int foilnum, int check);
    void SaveRooFig1D(TString NAME);
    // TH1D* histo; Int_t foilnum;
    // RooPlot *frameout() {return frame;}
};

extern double_t ranger[3];

#endif