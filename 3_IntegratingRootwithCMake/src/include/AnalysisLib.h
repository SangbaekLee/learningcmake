#ifndef __AnalysisLib_H__
#define __AnalysisLib_H__ 1

#include "plugin.h"
void NameSetting();

extern TString space;
extern TString underscore;
extern TString MeV;
extern TString deg;
extern TString dotroot;
extern TString GEMhitting;
extern TString outputfolder;
extern TString output;
extern TString fitting;

extern TString XRMStitle;
extern TString YRMStitle;

extern TString nameX;
extern TString nameY;
extern TString nameZ;
extern TString namedE;
extern TString nameE;
extern TString namefoilnum;
extern TString nameeventID;
extern TString nametracksize;
extern TString nametrackID;
extern TString namepID;
extern TString namehitID;
extern TString namehitsize;
extern TString nameangle;
extern TString name[3];

extern TString XYZ;
extern TString xz;
extern TString yz;
extern TString r;
extern TString z;
extern TString x;
extern TString y;
extern TString reco;

void SaveFig1D(int argc, TH1D* histo[],int setlog, TString name);
void SaveFig2D(int argc, TH2D* histo[],int setlog, TString name);
void SaveFig3D(int argc, TH3D* histo[],int setlog, TString name);
void SetHisto1D(TH1D* histo, TString a);
void SetHisto2D(TH2D* histo, TString a, TString b);
void SetHisto3D(TH3D* histo, TString a, TString b, TString c);

#endif
