#include "Roofitted.h"

double_t ranger[3] = {30.,100.,300.};

Roofitted::Roofitted(){}
Roofitted::Roofitted(TH1D* histo[3], TString name, int xory, int draw){
        
    Sethistofoilnum(histo);
    framefitted(1, xory);
    framefitted(2, xory);
    framefitted(3, xory);
    if (xory==0) name = name +(TString) "_X";
    if (xory==1) name = name +(TString) "_Y";
    if (draw!=0) SaveRooFig1D(name);

} // constructor with argument

Roofitted::~Roofitted(){}
void Roofitted::framefitted(int foilnum, int check){

       // Declare observable x
      // double_t range[3]={5.,10.,40.};
      RooRealVar x("x","x",-ranger[foilnum-1],ranger[foilnum-1]) ;

      // Create a binned dataset that imports contents of TH1 and associates its contents to observable 'x'
      RooDataHist dh("dh","dh",x,Import(*h[foilnum-1])) ;

      ostringstream os1;
      os1<<"Fitting result of Plane " <<foilnum;
      if (check==0) os1 <<", X direction";
      if (check==1) os1 <<", Y direction";

      // P l o t   a n d   f i t   a   R o o D a t a H i s t
      // ---------------------------------------------------
     // Make plot of binned dataset showing Poisson error bars (RooFit default)
      frameout[foilnum-1] = x.frame(Title((TString) os1.str())) ;
      frameout[foilnum-1]->SetYTitle("Frequency of Events");
      frameout[foilnum-1]->SetXTitle("Hitting Position (mm)");
      frameout[foilnum-1]->GetXaxis()->CenterTitle();
      frameout[foilnum-1]->GetYaxis()->CenterTitle();
      dh.plotOn(frameout[foilnum-1], MarkerSize(0.2)) ; 
      // gaussian - bkg f
      RooRealVar mean_b("mean_b","mean_b",0.,-1.,1.) ;
      RooRealVar sigma_b("sigma_b","sigma_b",30.,0.001,70.) ;
      RooGaussian gaussian("gaussian","gaussian",x,mean_b,sigma_b) ;

      // signal function for 1
      RooRealVar tau("tau","lifetime",0.1,0.001,1.) ;
      RooTruthModel truthm("truthm","truth model",x) ;
      RooRealVar mean_s("mean_s","mean_s",0.,-0.01,0.01) ;
      RooRealVar sigma_s("sigma_s","sigma_s",0.1,0.001,40.) ;
      RooGaussModel gaussm("gaussmodel","gaussmodel",x,mean_s,sigma_s) ;    
      RooDecay model("model","decay (x) gauss",x,tau,gaussm, RooDecay::DoubleSided) ;
      RooRealVar signal("signal", "signal yield", .4, 0., 1.);
      RooAddPdf sum("sum", "Double Gaussian", RooArgList(gaussm, gaussian), signal);
      RooFitResult* r;
      RooChi2Var* chi2_lowstat;
      if (foilnum==1){
        gStyle->SetOptFit(1111);

        r=sum.fitTo(dh,Save()) ;
        sum.plotOn(frameout[foilnum-1],MarkerSize(0.2)) ;
        // A d d   b o x   w i t h   p d f   p a r a m e t e r s 
        // -----------------------------------------------------
        frameout[foilnum-1]->getAttLine()->SetLineColor(kRed);

        // Left edge of box starts at 55% of Xaxis)
        sum.paramOn(frameout[foilnum-1],Layout(0.7,0.86,0.7)) ;
        frameout[foilnum-1]->getAttText()->SetTextSize(0.04) ;
      }
      else if (foilnum==2) {
        r=sum.fitTo(dh,Save()) ;
        sum.plotOn(frameout[foilnum-1]) ;
        frameout[foilnum-1]->getAttLine()->SetLineColor(kRed);
        // A d d   b o x   w i t h   p d f   p a r a m e t e r s 
        // -----------------------------------------------------
        // Left edge of box starts at 55% of Xaxis)
        sum.paramOn(frameout[foilnum-1],Layout(0.7,0.86,0.7)) ;
        frameout[foilnum-1]->getAttText()->SetTextSize(0.04) ;
       }
      else if (foilnum==3) {
        sum.fitTo(dh) ;
        sum.plotOn(frameout[foilnum-1]) ;
        frameout[foilnum-1]->getAttLine()->SetLineColor(kRed);
        // A d d   b o x   w i t h   p d f   p a r a m e t e r s 
        // -----------------------------------------------------
        // Left edge of box starts at 55% of Xaxis)
        sum.paramOn(frameout[foilnum-1],Layout(0.7,0.86,0.7)) ;
        frameout[foilnum-1]->getAttText()->SetTextSize(0.04) ;

     }
    frameout[foilnum-1]->SetMinimum(0.5);
    // frame[foilnum-1]->SetMaximum(100);
    // cout << " signal = " << s.getVal(x)<<"+-"<<s.getError() << endl ;
    // cout << " slope = " << tau.getVal(x) << endl ;
    // cout << " mean     = " << mean.getVal(x) << endl ;
    // cout << " sigma     = " << sigma.getVal(x) << endl ;
    value[foilnum-1]=sigma_s.getVal(x);
    error[foilnum-1]=sigma_s.getError();

}
void Roofitted::Sethistofoilnum(TH1D* histo[3]){
  h[0] = histo[0];
  h[1] = histo[1];
  h[2] = histo[2];
}
void Roofitted::SaveRooFig1D(TString NAME){
      TCanvas* canvas = new TCanvas("Canvas","Canvas",800,800) ;
      canvas->SetBatch(kTRUE);
      // if (setlog==1) canvas->SetLogy();
        canvas->Divide(1,3);
        (canvas->GetPad(1))->SetLogy();
        (canvas->GetPad(2))->SetLogy();
        (canvas->GetPad(3))->SetLogy();
        canvas-> cd(1);gPad->SetLeftMargin(0.15) ; frameout[0]->GetYaxis()->SetTitleOffset(1.4); frameout[0]->Draw("colz Hist") ;
        canvas-> cd(2);gPad->SetLeftMargin(0.15) ; frameout[1]->GetYaxis()->SetTitleOffset(1.4); frameout[1]->Draw("colz Hist") ;
        canvas-> cd(3);gPad->SetLeftMargin(0.15) ; frameout[2]->GetYaxis()->SetTitleOffset(1.4); frameout[2]->Draw("colz Hist") ;
        canvas-> SaveAs(NAME+(TString)".pdf");
}
