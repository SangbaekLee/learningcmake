#include "AnalysisLib.h"

//String Variables
TString space = " ";
TString underscore="_";
TString MeV = "MeV";
TString deg = "deg";
TString dotroot = ".root";
TString GEMhitting = "/Users/sangbaek/Storage/DarkLight/DL_software/rootfiles/Analyzed/GEM1CAngle_hitting_";
TString outputfolder = "Output/Averaged";
TString fitting="fitting";
TString output = ".pdf";
TString XRMStitle = ", X RMS Spread, 1st Plane";
TString YRMStitle = ", Y RMS Spread, 1st Plane";

TString nameX= "hit_pos_x";
TString nameY= "hit_pos_y";
TString nameZ= "hit_pos_z";
TString namedE= "hit_pos_dE";
TString nameE= "hit_pos_E";
TString namefoilnum = "hit_pos_foilnum";
TString nameeventID = "hit_pos_eventID";
TString nametracksize = "hit_pos_tracksize";
TString nametrackID = "hit_pos_trackID";
TString namepID = "hit_pos_pID";
TString namehitsize = "hit_pos_hitsize";
TString namehitID = "hit_pos_hitID";

TString name[3]={"1st Plane","2nd Plane", "3rd Plane"};

//for names of plots
TString XYZ = ", XYZ";
TString xz = ", xz";
TString yz = ", yz";
TString r = ", r";
TString z = ", z";
TString x = ", x";
TString y = ", y";
TString reco =", reco";
TString nameangle = "Angle Reco. ";

// Micellaneous void functions - to be saved 
void SaveFig1D(int argc, TH1D* histo[],int setlog, TString name){
    while (argc>0){

      TCanvas* canvas = new TCanvas("Canvas","Canvas",800,800) ;
      canvas->SetBatch(kTRUE);
      if (argc==1){
          if (setlog==1) canvas->SetLogy();        
          gPad->SetLeftMargin(0.15) ; histo[0]->GetYaxis()->SetTitleOffset(1.4); histo[0]->Draw("colz") ;
      }
      if (argc==3){
          canvas->Divide(1,3);
          if (setlog==1){
              (canvas->GetPad(1))->SetLogy();
              (canvas->GetPad(2))->SetLogy();
              (canvas->GetPad(3))->SetLogy();
          }
          canvas-> cd(1);gPad->SetLeftMargin(0.15) ; histo[0]->GetYaxis()->SetTitleOffset(1.4); histo[0]->Draw("colz Hist") ;
          canvas-> cd(2);gPad->SetLeftMargin(0.15) ; histo[1]->GetYaxis()->SetTitleOffset(1.4); histo[1]->Draw("colz Hist") ;
          canvas-> cd(3);gPad->SetLeftMargin(0.15) ; histo[2]->GetYaxis()->SetTitleOffset(1.4); histo[2]->Draw("colz Hist") ;
      }
      canvas-> SaveAs(name+output);
      delete canvas;
      break;
    }
}
void SaveFig2D(int argc, TH2D* histo[],int setlog, TString name){
    while (argc>0){

      TCanvas* canvas = new TCanvas("Canvas","Canvas",800,800) ;
      canvas->SetBatch(kTRUE);
      if (argc==1){
          if (setlog==1) canvas->SetLogz();
          gPad->SetLeftMargin(0.15) ; histo[0]->GetYaxis()->SetTitleOffset(1.4); histo[0]->Draw("colz") ;
      }
      if (argc==3){
        canvas->Divide(1,3);
          if (setlog==1){
              (canvas->GetPad(1))->SetLogz();
              (canvas->GetPad(2))->SetLogz();
              (canvas->GetPad(3))->SetLogz();
          }
        canvas-> cd(1);gPad->SetLeftMargin(0.15) ; histo[0]->GetYaxis()->SetTitleOffset(1.4); histo[0]->Draw("colz Hist") ;
        canvas-> cd(2);gPad->SetLeftMargin(0.15) ; histo[1]->GetYaxis()->SetTitleOffset(1.4); histo[1]->Draw("colz Hist") ;
        canvas-> cd(3);gPad->SetLeftMargin(0.15) ; histo[2]->GetYaxis()->SetTitleOffset(1.4); histo[2]->Draw("colz Hist") ;
      }
      canvas-> SaveAs(name+output);
      delete canvas;
      break;
    }
}

void SaveFig3D(int argc, TH3D* histo[], int setlog, TString name){
    while (argc>0){

      TCanvas* canvas = new TCanvas("Canvas","Canvas",800,800) ;
      canvas->SetBatch(kTRUE);
      if (setlog==1) cout<<"Setting log scale in 3d, not an effective way - do nothing\n";
      if (argc==1){
       gPad->SetLeftMargin(0.15) ; histo[0]->GetYaxis()->SetTitleOffset(1.4); histo[0]->Draw("colz") ;
      }
      if (argc==3){
        canvas->Divide(1,3);
        canvas-> cd(1);gPad->SetLeftMargin(0.15) ; histo[0]->GetYaxis()->SetTitleOffset(1.4); histo[0]->Draw("colz Hist") ;
        canvas-> cd(2);gPad->SetLeftMargin(0.15) ; histo[1]->GetYaxis()->SetTitleOffset(1.4); histo[1]->Draw("colz Hist") ;
        canvas-> cd(3);gPad->SetLeftMargin(0.15) ; histo[2]->GetYaxis()->SetTitleOffset(1.4); histo[2]->Draw("colz Hist") ;
      }
      canvas-> SaveAs(name+output);
      delete canvas;
      break;
    }
}

void SetHisto1D(TH1D* histo, TString a){
    histo->GetXaxis()->SetTitle(a);
    histo->GetXaxis()->CenterTitle();
    histo->GetYaxis()->CenterTitle();
}

void SetHisto2D(TH2D* histo, TString a, TString b){
    histo->GetXaxis()->SetTitle(a);
    histo->GetXaxis()->CenterTitle();
    histo->GetYaxis()->SetTitle(b);
    histo->GetYaxis()->CenterTitle();
}

void SetHisto3D(TH3D* histo, TString a, TString b, TString c){
    histo->GetXaxis()->SetTitle(a);
    histo->GetXaxis()->CenterTitle();
    histo->GetYaxis()->SetTitle(b);
    histo->GetYaxis()->CenterTitle();
    histo->GetZaxis()->SetTitle(c);
    histo->GetZaxis()->CenterTitle();
}