#include "Main.h"
#include "AnalysisLib.h"
#include "Roofitted.h"
int main()
{
  narray = 5; // Set up momentum and angle parameters to be processed
  Double_t Momarray[narray];
  Double_t anglearray[narray];
  for (int i=0; i<narray; i++ ){
    Momarray[i]=24+3*i;
    anglearray[i]=21+9*i;
  }

  binnumber = 2; // control binnumber if needed for defining histogram
  double_t rangez[2] = {1, 4}; // draw z from center -1 to center +4


  // gROOT->SetBatch(kTRUE); // uncomment if the code should be run by root - but I'd rather build the code.

  // Set up angle resolution plot by angle - angle
  histothetareco2 = new TH2D("Angle Reconstruction","Angle Reconstruction",90,0,90,90,0,90);
  SetHisto2D(histothetareco2,"#theta_{true} (deg)","#theta_{reco} (deg)");
  //Set up angle resolution plot by momentum, angle
  histothetareco3 = new TH2D("Angular Spread","Angular Spread",7,24-1.5-3,36+1.5+3,7,21-4.5-9,57+4.5+9);
  SetHisto2D(histothetareco3,"Momentum (MeV/c)","Impact Angle (deg)");
  histothetareco3->GetZaxis()->SetRangeUser(1.3,2.1);

  histosanity = new TH1D("Sanity Check","Sanity Check", 100000,0,100000);
  SetHisto1D(histosanity, " ");

  int alltracks=0;
  int primarytracks=0;
  int primarytrackswithh1=0;
  int primarytrackswithh2=0;
  int primarytrackswithh3=0;
  int primarytrackswithh1h2=0;
  int primarytrackswithh2h3=0;
  int primarytrackswithh1h3=0;
  int primarytrackswithh1h2h3=0;
  int secondarytracks=0;
  int secondarytrackshavingtwohits=0;
  int secondarytrackshavingtwohitsovertwofoils=0;

  //Open a text file for angle, momentum, RMS, erros... of each setting
  // outfile.open("Spread.txt");
  // outfile << "Impact angle\t"<< "Momentum (MeV/c)\t"<<"X RMS\t"<<"X RMS Error\t"<<"Y RMS\t"<<"Y RMS Error\t"<<"Plane No.";

  Roofitted *roof1, *roof2; //class from roofitted.cxx, roof1 for x roof2 for y technically

  // for (int angleindex=0; angleindex<5; angleindex++){ //to draw rms - momentum
  for (int momindex=0; momindex<narray; momindex++){
    for (int angleindex=0; angleindex<narray; angleindex++){ //to draw rms-angle

        osmomentum<<fixed<<setprecision(1)<<Momarray[momindex]; // use stringstream to convert float to string.
        osangle<<fixed<<setprecision(1)<<anglearray[angleindex];
        Momentum = (TString) osmomentum.str(); // I know it's bad to not use STL but TString spoils me.
        Angle = (TString) osangle.str();

        filename = GEMhitting+Momentum+MeV+Angle+deg+dotroot; // because I can add string like a piece of cake
        f = new TFile (filename);
        
        histoX = (TH1D*) f->Get(nameX);  //restore histograms in the root files
        histoY = (TH1D*) f->Get(nameY);
        histoZ = (TH1D*) f->Get(nameZ); // global coord's.
        histoEdep = (TH1D*) f->Get(namedE);//kinetic energy
        histoEk = (TH1D*) f->Get(nameE);// energy deposition
        histofoilnum = (TH1I*) f->Get(namefoilnum); // foil number of hit
        histoeventID = (TH1I*) f->Get(nameeventID); // event ID
        histotracksize = (TH1I*) f->Get(nametracksize); // total track numbers per event
        histotrackID = (TH1I*) f->Get(nametrackID); //track ID of specific hit
        histopID = (TH1I*) f->Get(namepID);  // parent track number if secondary
        histohitsize = (TH1I*) f->Get(namehitsize); // hit number per track
        histohitID = (TH1I*) f->Get(namehitID); //hit id in the track
        t=anglearray[angleindex]; //temporary impact angle

        theta=(90.-t)*M_PI/180.; // use reciprocal of impact angle 

        for (int i=0; i<3; i++){
          //GEM consists of effective region 1(9.31 cm), active region (3 cm), effective region 2(3.075 cm) - this makes 9.31 - (9.31+3+3.075)/2 = 1.6175 separation from the plane's center
          //If details of foils change, please change this number accordingly.
          //centerx, y, z for histograms and analytic calc.
          centerx[i]=0.;
          centerz[i]=50.*i+1.6175; // The center of 1st foil is supposed to be at origin regardless of the length - correct 1.6175 mm.
          centery[i]=50.*tan(theta)*i+1.6175*tan(theta); 

          //Naming histogram for drawing
          nameXYZ[i]= name[i]+Angle+deg+Momentum+MeV+XYZ;
          namexz[i] = name[i]+Angle+deg+Momentum+MeV+xz;
          nameyz[i] = name[i]+Angle+deg+Momentum+MeV+yz;
          namer[i] = name[i] +Angle+deg+Momentum+MeV+r;
          namez[i] = name[i] +Angle+deg+Momentum+MeV+z;
          namex[i] = name[i] +Angle+deg+Momentum+MeV+x;
          namey[i] = name[i] +Angle+deg+Momentum+MeV+y;
          //Set up histograms with those names
          // histoXYZ[i]= new TH3D(nameXYZ[i],nameXYZ[i],(int) binnumber* ranger[i],-ranger[i],+ranger[i],(int) binnumber* ranger[i],-ranger[i],+ranger[i],(int) binnumber* ranger[i],50/cos(theta)*(i-1)-100,50/cos(theta)*(i-1)+100);
          // SetHisto3D(histoXYZ[i],"X (mm)","Y (mm)","Z (mm)");
          
          histoxz[i] = new TH2D(namexz[i], namexz[i],(int) binnumber* ranger[i], -ranger[i],+ranger[i],(int) binnumber* ranger[i],-rangez[0],+rangez[1]);
          SetHisto2D(histoxz[i],"X (mm)" ,"Z (mm)");

          histoyz[i] = new TH2D(nameyz[i], nameyz[i],(int) binnumber* ranger[i], -ranger[i],+ranger[i],(int) binnumber* ranger[i],-rangez[0],+rangez[1]);
          SetHisto2D(histoyz[i],"Y (mm)","Z (mm)");

          histor[i] = new TH1D(namer[i],namer[i],(int) binnumber* ranger[i],0.,+binnumber*ranger[i]);
          SetHisto1D(histor[i],"R (mm)");

          histoz[i] = new TH1D(namez[i],namez[i],(int) binnumber* ranger[i],-rangez[0],+rangez[1]);
          SetHisto1D(histoz[i],"Z (mm)");

          histox[i] = new TH1D(namex[i],namex[i],(int) binnumber* ranger[i],-ranger[i],+ranger[i]);
          SetHisto1D(histox[i],"X (mm)");
          histox[i]->GetYaxis()->SetRangeUser(0.1,100000);

          histoy[i] = new TH1D(namey[i],namey[i],(int) binnumber* ranger[i],-ranger[i],+ranger[i]);
          SetHisto1D(histoy[i],"Y (mm)");
          histoy[i]->GetYaxis()->SetRangeUser(0.1,100000);
        }
        //set up histo
        nameangle=nameangle+Angle+deg+Momentum+MeV;
        histothetareco = new TH1D(nameangle,nameangle,200,0,30);
        SetHisto1D(histothetareco,"#theta_{reco}-#theta_{true} (deg)" );
        //integers for counting events
        notgoodcases = 0; // count events when hits in some foiis missing
        countevent=0; // count entire events
        counthit=0; //count hits in the same event
        Int_t countpoint=0; //count hits in the entire events  - just for debugging

        Int_t Entries = histoX->GetEntries();


        // Main data processing
        for (int i=0; i<Entries; i++){

                // // Now average physical quantity with weight Edep for the same track, for the same foil
                // if (i>0 && histoeventID->GetBinContent(i)==histoeventID->GetBinContent(i-1)) continue; //skip if it's not the first hit of each event
                // // Skip if there's no hit in specific event
                // if (histoeventID->GetBinContent(i)>histoeventID->GetBinContent(i-1)+1){
                //   cout<<"No hit in the events between"<<histoeventID->GetBinContent(i-1)<<" and "<<histoeventID->GetBinContent(i)<<endl;
                //   cout<<"But such events generated"<<endl;
                //   notgoodcases++;
                //   countevent++;
                // }
                // counthit=0;
                // countevent++;

                // // Count the hits in the event
                // for (int j=i ; j<Entries; j++){
                //     if (histoeventID->GetBinContent(j)!=histoeventID->GetBinContent(i)) break;
                //     counthit++;
                // }
                // checkfoil[0]=0, checkfoil[1]=0, checkfoil[2]=0; //initialize checker

                // for (int hitindex=0; hitindex<counthit; hitindex++){

                //     // cout<<eventID[i+hitindex]<<"\t"<<histofoilnum->GetBinContent(i+hitindex)<<endl;

                //   for (int k=0; k<3; k++){
                //     if (histofoilnum->GetBinContent(i+hitindex)==k+1) checkfoil[k]+=1;
                //   }
                // }
                
                // if (checkfoil[0]+checkfoil[1]+checkfoil[2] != counthit){ 
                //     cout<<"Significant Error"<<endl;
                //     break;
                // }
                // countpoint+= counthit;
                // // skip if there's any hit not hitting foil1, foil2 and foil3...
                // if (checkfoil[0]*checkfoil[1]*checkfoil[2]==0){
                //   notgoodcases++;
                //   continue;
                // }

                //Sanity Check
                // if (i ==0 | histoeventID->GetBinContent(i)>histoeventID->GetBinContent(i-1)) alltracks += histotracksize->GetBinContent(i);
                // if (i>0 )


                // AverageX[0]=0, AverageX[1]=0, AverageX[2]=0;
                // AverageY[0]=0, AverageY[1]=0, AverageY[2]=0;
                // AverageZ[0]=0, AverageZ[1]=0, AverageZ[2]=0;
                // AverageR[0]=0, AverageR[1]=0, AverageR[2]=0;
                // TotEdep[0]=0, TotEdep[1]=0, TotEdep[2]=0;

                // for (int hitindex = 0; hitindex<counthit; hitindex++){                  
                //    AverageX[(int) histofoilnum->GetBinContent(i+hitindex)-1]+= histoX->GetBinContent(i+hitindex)*histoEdep->GetBinContent(i+hitindex);
                //    AverageY[(int) histofoilnum->GetBinContent(i+hitindex)-1]+= histoY->GetBinContent(i+hitindex)*histoEdep->GetBinContent(i+hitindex);
                //    AverageZ[(int) histofoilnum->GetBinContent(i+hitindex)-1]+= histoZ->GetBinContent(i+hitindex)*histoEdep->GetBinContent(i+hitindex);
                //    TotEdep[(int) histofoilnum->GetBinContent(i+hitindex)-1]+= histoEdep->GetBinContent(i+hitindex);
                //    // skip if there is no energy deposition in each foil in each event
                //     if (hitindex==counthit-1 && TotEdep[0]*TotEdep[1]*TotEdep[2]==0){
                //       notgoodcases++;
                //       continue;
                //     }
                //   }

                // for (int k=0; k<3;k++){
                //   AverageX[k] = AverageX[k]/TotEdep[k];
                //   AverageY[k] = AverageY[k]/TotEdep[k];
                //   AverageZ[k] = AverageZ[k]/TotEdep[k];
                //   // histoXYZ[k]->Fill(AverageX[k],AverageY[k],AverageZ[k]);
                //   foilhit[k].SetXYZ(AverageX[k], AverageY[k], AverageZ[k]);
                // //   // if ((AverageZ-centerz[0])/(3.)<=0.001) continue;
                //   AverageR[k] = Sqrt(foilhit[k].X()*foilhit[k].X()+foilhit[k].Y()*foilhit[k].Y()/cos(theta)/cos(theta));
                //   AverageY[k] = cos(theta)*foilhit[k].Y()+sin(theta)*foilhit[k].Z(); //Rotation
                //   AverageZ[k] = -sin(theta)*foilhit[k].Y()+cos(theta)*foilhit[k].Z();
                //   //Fill the histogram with averaged point
                  
                //   histoxz[k]->Fill(AverageX[k]-centerx[k], AverageZ[k]-centerz[k]);
                //   histoyz[k]->Fill(AverageY[k]-centery[k], AverageZ[k]-centerz[k]);
                //   histor[k]->Fill(AverageR[k]);
                //   histoz[k]->Fill(AverageZ[k]-centerz[k]);
                //   histox[k]->Fill(AverageX[k]-centerx[k]);
                //   histoy[k]->Fill(AverageY[k]-centery[k]);                
                // }

                // theta_reco=atan((foilhit[1]-foilhit[0]).Perp()/(foilhit[1]-foilhit[0]).Z());
                // theta_reco=radtodeg*theta_reco;

                // histothetareco->Fill(theta_reco);
                // histothetareco2->Fill(anglearray[angleindex],theta_reco+anglearray[angleindex]);
          }
        //fill angular resoltuion (rms)
        // histothetareco3->Fill(Momarray[angleindex],anglearray[momindex],histothetareco->GetRMS());

        // cout<<"When we have "<< countevent<<" generated events hitting at "<<Momentum<<"MeV"<<"\t"<<Angle<<"deg"<<endl;//<<XRMS[angleindex][k]<<"\n"<<YRMS[angleindex][k]<<"\n";
        // cout<<"How many times we can't reconstruct? "<<"\t"<<notgoodcases<<" out of "<<countevent<<" total hits"<<endl;//<<XRMS[angleindex][k]<<"\n"<<YRMS[angleindex][k]<<"\n";
        // cout<<"which is "<< 100.* notgoodcases/(1.*countevent) <<" %"<<endl;


        // data processing ended, belows are just drawing

        // Draw and save histograms, histograms' names are self-explanatory
        // SaveFig3D(3,histoXYZ,0,outputfolder+Angle+deg+Momentum+MeV+XYZ);
        // SaveFig2D(3, histoxz, 1, outputfolder+Angle+deg+Momentum+MeV+xz);
        // SaveFig1D(3, histoz, 1,outputfolder+Angle+deg+Momentum+MeV+z);
        // SaveFig1D(3, histor, 1,outputfolder+Angle+deg+Momentum+MeV+r);
        // SaveFig1D(3, histox, 1,outputfolder+Angle+deg+Momentum+MeV+x);
        // SaveFig1D(3, histoy, 1,outputfolder+Angle+deg+Momentum+MeV+y);
        // SaveFig2D(3, histoyz, 1, outputfolder+Angle+deg+Momentum+MeV+yz);
        // cache1D[0]=histothetareco;
        // SaveFig1D(1, cache1D,1, outputfolder+Angle+deg+Momentum+MeV+reco);

        // fitting and draw(controlled by the last argument)
        // roof1 = new Roofitted(histox,outputfolder+Angle+deg+Momentum+MeV+fitting,0,1);// fit X //change the last arg. to 0 if drawing to be skipped
        // roof2 =new Roofitted(histoy,outputfolder+Angle+deg+Momentum+MeV+fitting,1,1);// fit Y
        // write fitting parameter in text file. python script "drawplot.py" will be in charge of drawing after this.
        // outfile <<"\n"<<Angle<<"\t"<< Momentum;
        // for (int k=0;k<3;k++){
        // outfile<<"\t"<<roof1->value[k]<<"\t"<<roof1->error[k]<<"\t"<<roof2->value[k]<<"\t"<<roof2->error[k]<<"\t"<<k+1;
        // }

        osmomentum.str(""); osmomentum.clear();
        osangle.str(""); osangle.clear();

      }

  }
    // cache2D[0]=histothetareco3;
    // SaveFig2D(1,cache2D,0,(TString) "angular");

    // cache2D[0] =histothetareco2;
    // SaveFig2D(1,cache2D,1,(TString) "anglereconstruction");

    // outfile.close();

    return 0;
    //end of main
}