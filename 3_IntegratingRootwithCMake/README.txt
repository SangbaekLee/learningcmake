1. 이번에는 좀 src, lib, include 폴더를 사용해서 정돈된 프로그래밍의 가장 기초를 연습해보자.
이번 코드는 input 파일들이 없으니 당연히 안 돌아가겠지만 make -j4 까지는 될거야.

2. 백문이 불여일견
mkdir build
cd build
ccmake ../src  -> c -> g
(or cmake ../src)
make -j4
./AnalysisLib

3.
CMakeLists.txt는 src에 있어

1,2번 예제와 가장 큰 특징으로는 shared library 를 만드는 부분이지 (line 19~22)
그리고 둘다 라이브러리로 Roofit을 넣어줬고.

보면 이해가 가긴 할듯

앞으로는 subdirectory가 많아졌을 때 CMakeLists.txt를 짜는 법이 궁금할텐데,

https://stackoverflow.com/questions/8304190/cmake-with-include-and-source-paths-basic-setup

를 보면서 각 코드들이 있는 하위폴더들에 CMakeLists.txt를 만들고 가장 상위폴더에 add_subdirectory()를 이용해서 가지치기를 해나가면 더 정돈된 코딩이 가능할 듯.

예제 끝!