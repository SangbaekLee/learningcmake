1. 이번 예제에서는 엄청 간단한 루트파일 하나를 빌드해볼건데,
https://root.cern.ch/how/integrate-root-my-project-cmake
에서 CMakeLists.txt를 그대로 가져왔어

소스는 https://root.cern.ch/root/html/tutorials/foam/foam_kanwa.C.html 에서 가져왔어.

2. 백문이 불여일견 아래를 해보자.

mkdir build
cd build
ccmake ../A1  -> c -> g
(or cmake ../A1)
make -j4
./Main

3.
이번에는 A1이라는 폴더를 만들어서 CMakeLists.txt 를 놓았더니 폴더가 조금 보기 편해지네.
CMakeLists.txt 에서 16번째 줄에
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)
가 있는데, 어차피 A1/include에는 아무것도 없으니 상관은 없어.

그리고 마지막에 Foam 이라는 거는 TFoam.h를 말하는 건데 저게 없어서 에러가 나더라고.
우리 포닥한테 물어보니까 저걸 CMake 에 수동으로 넣어야한대.
Roofit같은 경우에도 마찬가지고.

그럼 3번 예제로 넘어갈까